//--------------------------------------------------------------------------------------------//
// MASTER MIND
// Ahc�ne TRIKI
// DATE : 14/02/2024
//-------------------------------------------------------------------------------------------//
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#define TAILLE 4                    // Taille des tableaux


//--------------------------------- fonction qui permet de tirer un chiffre al�atoire entre 0 et 10 .
int my_rand (void){

    static int first = 0;
    if (first == 0){
        srand (time (NULL));
        first = 1;
    }
    return (rand ()% 10);
}

//------------------------------- fonction qui cr�e le code � 4 chiffres � trouver.
void code_depart(int *code){

    for (int i = 0;i<TAILLE; i++){
        code[i]= my_rand();
    }
}

//------------------------------- fonction qui permet de rentr�e un code � 4 chiffres.
void trouver_code(int *code_recherche) {

    printf("\nEntrez un code a 4 chiffres : ");
    scanf("%1d%1d%1d%1d", &code_recherche[0], &code_recherche[1], &code_recherche[2], &code_recherche[3]);

}

//------------------------------- fonction qui compare le code rentr� et le code � trouv�
bool comparaison(int *entrer ,int *reponse){

    int bp = 0;
    int mp = 0;
    int verifie [4]={0};

    for (int i=0;i<TAILLE;i++){
        if (entrer[i]== reponse[i]){
            bp = bp + 1;
            verifie[i]=1;
        }
    }

    for (int i=0;i<TAILLE;i++){
            for (int j=0; j<TAILLE;j++){
                    if(entrer[i]==reponse[j]&& verifie[i]!=1){
                        mp = mp + 1;
                        verifie[i]=1;

                    }
            }
    }

    if (bp == TAILLE){

        printf("vous avez trouve le bon code !!!! ");
    }
    else {

        if(bp > 1){
            printf("il y a %d chiffres bien places\n", bp);
        }
        else {
            printf("il y a %d chiffre bien place\n", bp);
        }
        if(mp > 1){
            printf("il y a %d chiffres mal places\n", mp);
        }
        else{
            printf("il y a %d chiffre mal place\n", mp);
        }
    }

    return (bp == 4);
}

/*
 Main
*/
int main (void){

    int code_secret[4];
    int code_recherche[4];
    int essais;
    printf("****************************************\n");
    printf("*************MASTER MIND****************\n");
    printf("****************************************\n\n");
    printf("Le but du jeu est de trouver un code � 4 chiffres.");
    code_depart(code_secret);
    printf("\n");

    bool isOk = false;
    while (isOk == false ){

        trouver_code(code_recherche);
        isOk = comparaison(code_secret,code_recherche);
        essais = essais + 1;
    }

    if (essais == 1){
        printf ("vous avez mis %d essai ",essais);
    }
    else {
        printf ("vous avez mis %d essais ",essais);
    }

    printf("\nFIN !");
}
